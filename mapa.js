let miMapa = L.map('mapid');

miMapa.setView([4.74304081421927, -74.0180303142636], 16);

let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
});
miProveedor.addTo(miMapa);

let miMarcador = L.marker([4.743875633344572,-74.02075633485316]);
miMarcador.addTo(miMapa)

var circle = L.circle([4.74304081421927, -74.0180303142636], {
    color: 'blue',
    fillColor: 'purple',
    fillOpacity: 0.5,
    radius: 460
});
circle.addTo(miMapa);

var polygon = L.polygon([
    [4.7418608200514, -74.01496976175912],
    [4.743040814308486, -74.01390729016725],
    [4.742314489134756, -74.01259045638488],
    [4.741692884628165, -74.0121411524707],
    [4.741071279551505, -74.01290761209765],
    [4.7418608200514, -74.01496976175912]
],{
    color:'yellow'
});
polygon.addTo(miMapa)